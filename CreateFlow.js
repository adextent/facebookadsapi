var fbModules = require('./modules/modules.js');
var common = require('./modules/common.js');

var account = fbModules.initAccount(19866516); //initiation of account object

//break with async

var props = {
    name: 'Etay Test Campaign',
    OBJECTIVE: 'MOBILE_APP_INSTALLS',
    campaign_group_status: 'PAUSED'
};

console.log("Creating campaign...");
account.createCampaign(props)
    .then(function (response) {
        console.log("Success, its id is " + response.sdk.id);
        var newCampaign = response;

        console.log("Creating adSet for the campaign...");
        var adSetProps = {
            name: 'etayTestADSet',
            billing_event: 'APP_INSTALLS',
            targeting: {
                "age_max": 65,
                "age_min": 22,
                "geo_locations": {"countries": ["ID"], "location_types": ["home", "recent"]},
                "page_types": ["mobileexternal", "mobilefeed"],
                "user_device": ["Android_Smartphone"],
                "user_os": ["Android"]
            },
            bid_amount: '1',
            optimization_goal: 'APP_INSTALLS',
            dailyBudget: '500',
            objective: 'APP_INSTALLS',
            promotedObject: {
                "application_id": "1599981143593595",
                "object_store_url": "http://play.google.com/store/apps/details?id=com.mgyun.supercleaner"
            }
        };

        newCampaign.createAdSet(adSetProps)
            .then(function (response) {
                var newAdSet = response;
                console.log("Created ad set, its id is " + newAdSet.sdk.id);
                console.log("Uploading image for creative...");

                var props = {name: 'test.png'};
                var imageName = props.name;
                var stream = {filename: 'test.png'};
                account.uploadImage(props, stream)
                    .then(function (response) {
                        var imagehash = response[imageName].hash;
                        console.log("Creating creative for ad...");

                        var props = {
                            name: 'Etay Test Creative2',
                            object_story_spec: {
                                "page_id": "493868817458970",
                                "link_data": {
                                    link: "http://play.google.com/store/apps/details?id=com.mgyun.supercleaner",
                                    message: "test",
                                    caption:"test",
                                    call_to_action: {
                                        type: "INSTALL_MOBILE_APP",
                                        value: {link:"http://play.google.com/store/apps/details?id=com.mgyun.supercleaner", link_caption:'test'}
                                    },
                                    image_hash: imagehash
                                }
                            }
                        };

                        account.createCreative(props)
                            .then(function (response) {
                                var newCreative = response;
                                console.log("Will create ad for the campaign using creative id " + newCreative.sdk.id);

                                var adProps = {
                                    name: 'etayTestAD',
                                    creative: {"id": newCreative.sdk.id},
                                    conversionSpecs: [{
                                        "action.type": ["mobile_app_install"],
                                        "application": ["1599981143593595"]
                                    }],
                                    bidAmount: '35',
                                    bidType: 'ABSOLUTE_OCPM',
                                    bidInfo: {"ACTIONS": 35},
                                    targeting: {
                                        "age_max": 65,
                                        "age_min": 22,
                                        "geo_locations": {
                                            "countries": ["ID"],
                                            "location_types": ["home", "recent"]
                                        },
                                        "page_types": ["mobileexternal", "mobilefeed"],
                                        "user_device": ["Android_Smartphone"],
                                        "user_os": ["Android"]
                                    },
                                    trackingSpecs: [{
                                        "action.type": ["post_engagement"],
                                        "page": ["493868817458970"],
                                        "post": ["493869080792277"]
                                    }, {
                                        "action.type": ["app_custom_event"],
                                        "application": ["1599981143593595"]
                                    }]
                                };

                                newAdSet.createAd(adProps)
                                    .then(function (response) {
                                        var newAd = response;
                                        console.log("Done.");
                                    })
                                    .catch(function (error) {
                                        console.log(error);
                                    });
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            })
            .catch(function (error) {
                console.log(error);
            });
    })
    .catch(function (error) {
        console.log(error);
    });

