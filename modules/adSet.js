/**
 * Created by etayl on 9/21/15.
 * https://developers.facebook.com/docs/marketing-api/adset/pacing/v2.4
 */
var Q = require('q');
var ad_lib = require('./ad.js');


exports.adSet=function(adSet) {
    this.sdk = adSet;

    this.getInfo = function (fields) {
        var deferred = Q.defer();
        var adSetInfo = this.sdk.get().require.apply(this, fields);
        adSetInfo.done()
            .then(function (response) {
                console.log("Success with getting info about specific adset, sending back results.");
                deferred.resolve(response.data);
            })
            .catch(function (error) {
                console.log("Troubles with getting campaign's info.");
                deferred.reject(error);
            })
        return deferred.promise;
    };

    this.getAds=function(fields){
        var deferred = Q.defer();
        var adSetAds = this.sdk.getAds().require.apply(this, fields);
        adSetAds.done()
            .then(function (response) {
                console.log("Success with getting adset's ads, sending back results.");
                deferred.resolve(response.data);
            })
            .catch(function (error) {
                console.log("Troubles with getting campaign's info.");
                deferred.reject(error);
            })
        return deferred.promise;
    }

    this.createAd = function (props) {
        var deferred = Q.defer();

        var CreateAd = this.sdk.createAd().set(props);
        CreateAd.done()
            .then(function (response) {
                console.log("Success with creating new ad in campaign, sending back new ad object.");
                var adWrapper = new ad_lib.ad(response.data);
                console.log("Ad initiated, sending back an object with functions.");
                deferred.resolve(adWrapper);
            })
            .catch(function (error) {
                console.log("Troubles with creating Ad in AdSet.");
                deferred.reject(error);
            })
        return deferred.promise;
    };
}
