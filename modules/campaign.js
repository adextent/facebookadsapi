/**
 * Created by etayl on 9/21/15.
 */
var Q = require('q');
var adSet_lib = require('./adSet.js');


exports.campaign=function(adCampaign) {
    this.sdk = adCampaign;

    this.getInfo = function (fields) {
        var deferred = Q.defer();
        var campaignInfo = this.sdk.get().require.apply(this, fields);
        campaignInfo.done()
            .then(function (response) {
                console.log("Success with getting info about specific campaign, sending back results.");
                deferred.resolve(response.data);
            })
            .catch(function (error) {
                console.log("Troubles with getting campaign's info.");
                deferred.reject(error);
            })
        return deferred.promise;
    };

    this.getAdSets=function(fields){
        var deferred = Q.defer();
        var campaignAdSets = this.sdk.getAdSets().require.apply(this, fields);
        campaignAdSets.done()
            .then(function (response) {
                console.log("Success with getting campaign's adSets, sending back results.");
                deferred.resolve(response.data);
            })
            .catch(function (error) {
                console.log("Troubles with getting campaign's info.");
                deferred.reject(error);
            })
        return deferred.promise;
    }

    this.createAdSet = function (props) {
        var deferred = Q.defer();
        var CreateAdSet = this.sdk.createAdSet().set(props);
        CreateAdSet.done()
            .then(function (response) {
                console.log("Success with creating new AdSet in campaign, sending back new AdSet object.");
                var adSetWrapper = new adSet_lib.adSet(response.data);
                console.log("AdSet initiated, sending back an object with functions.");
                deferred.resolve(adSetWrapper);
            })
            .catch(function (error) {
                console.log("Troubles with creating AdSet in campaign.");
                deferred.reject(error);
            })
        return deferred.promise;
    };

    this.getInsights = function (props) {
        //https://developers.facebook.com/docs/marketing-api/insights/v2.4
        var deferred = Q.defer();
        var getStats= this.sdk.getInsights().set(props);
        //console.log(this.sdk.getInsights.toString());
        getStats.done()
            .then(function (response) {
                console.log("Success with getting stats for campaign, sending back new AdSet object.");

                if (response.paging){
                    paging(response.data,response.next);

                    function paging(data,next){

                        next()
                            .then(function(response){
                                console.log("Paging:"+response.paging+" pageSize:"+response.pageSize+" PageIndex:"+response.pageIndex+" with "+data.length+" objects.");
                                console.log("joining..");
                                var alldata=data.concat(response.data);
                                if (response.paging && data.length==25){
                                    paging(alldata,response.next);
                                }
                                else{
                                    deferred.resolve(alldata);
                                }
                            })
                            .catch(function (error) {
                                console.log("Troubles with joining,");
                                deferred.reject(error);
                            })
                    }
                }
                else{
                    deferred.resolve(response.data);
                }
            })
            .catch(function (error) {
                console.log("Troubles with getting stats for campaign.");
                deferred.reject(error);
            })
        return deferred.promise;
    };

}
