/**
 * Created by etayl on 9/21/15.
 */

var Q = require('q');
var creative_lib = require('./creative.js');

exports.ad = function (ad) {
    this.sdk = ad;

    this.getInfo = function (fields) {
        var deferred = Q.defer();
        var adSetInfo = this.sdk.get().require.apply(this, fields);
        adSetInfo.done()
            .then(function (response) {
                console.log("Success with getting info about specific ad, sending back results.");
                deferred.resolve(response.data);
            })
            .catch(function (error) {
                console.log("Troubles with getting ad's info.");
                deferred.reject(error);
            })
        return deferred.promise;
    }

    this.getAdCreative = function (fields) {
        var deferred = Q.defer();
        var adCreatives = this.sdk.getCreatives().require.apply(this, fields);
        adCreatives.done()
            .then(function (response) {
                console.log("Success with getting ad's creatives, sending back results.");
                deferred.resolve(response.data);
            })
            .catch(function (error) {
                console.log("Troubles with getting campaign's info.");
                deferred.reject(error);
            })
        return deferred.promise;
    }
}
