//get graph tocken:
//https://developers.facebook.com/tools/explorer/167617240100890?method=GET&path=6030230454812%3Ffields%3Dname&version=v2.4
//or create one that never expries with non-etay user, using:
//https://developers.facebook.com/tools/debug/accesstoken?q=CAACYcm4N8BoBAA4z7R24F68gDQZBXbZA7NrSk8HL3SIUbiLhXqVoj9fhEuqv5Q1lc2qs1jhi9HSJnF34wL7jmDkHKdp3zjfrrmvdTHaFEfZBKYfmhFegcmShXHOV6ChZCVQomHI2KW7jRKxz172D2nR6bznE1zsz3kK1Mc1h7l2SneupSBGY1UhqZCTinY7ipDFUxW63CWgZDZD
//references:
//https://developers.facebook.com/docs/graph-api/reference/
//https://developers.facebook.com/docs/marketing-api/mobile-app-ads/v2.4

var adsAPI = require('../FacebookAdsSDK/adsAPI.js');
//Etay:
//var accessToken = 'CAACYcm4N8BoBACjwcHO37ZA0J7zgFCrwkNpMZBiNffv0AZBBuLj9yEpHLjVo4MQ1qQCaQjMPizLikSCVA5eSAUrIU3xA4LGZBakQBKvyXZC2yJRCvC8yrhFStUd7f0S4L3ZCe2mmpNo34ZCt3DZCyKZCaStDHFNBNeC8jkDHvBQZBG3rhQP6HLXchT0ytSDwgUCzUZD';
//Ron:
var accessToken = 'CAACYcm4N8BoBAHS4WSeJ5r7ScBqFHPZCdjzqqjsXmx0L6UjvRd4tDpGCP7HLqKICV5u9bvjdyjhq95zAtE4QoHYFI3wcCbcpkBewQkjnvJnZCtXgNlxZCkqaDiOGLge408RnDhGZBFSrYZBhauA1Lc0b6RNU0Ymd3j7ge42HbOoCZACuZCYZCt2Vtxm5rxH397wZD';
var adsAPIVersion = 'v2.4';

var creative_lib = require('./creative.js');
var ad_lib = require('./ad.js');
var adSet_lib = require('./adSet.js');
var campaign_lib = require('./campaign.js');
var account_lib = require('./account.js');

exports.initAccount = function (adAccountId) {
    var adAccount = adsAPI(adsAPIVersion, accessToken).getAdAccount(adAccountId);
    var accountWrapper = new account_lib.account(adAccount);
    console.log("Account initiated, sending back an object with functions.");
    return accountWrapper;
};

exports.initCampaign = function (adAccount, campaignId) {
    var adCampaign = adAccount.sdk.getAdCampaign(campaignId);
    var campaignWrapper = new campaign_lib.campaign(adCampaign);
    console.log("Campaign initiated, sending back an object with functions.");
    return campaignWrapper;
};

exports.initAdSet = function (adAccount, adSetID) {
    var adSet = adAccount.sdk.getAdSet(adSetID);
    var adSetWrapper = new adSet_lib.adSet(adSet);
    console.log("AdSet initiated, sending back an object with functions.");
    return adSetWrapper;
};

exports.initAd = function (adAccount, adID) {
    var ad = adAccount.sdk.getAd(adID);
    var adWrapper = new ad_lib.ad(ad);
    console.log("Ad initiated, sending back an object with functions.");
    return adWrapper;
};



exports.initCreative = function (adAccount, CreativeID) {
    var creative = adAccount.sdk.getCreative(CreativeID);
    var creativeWrapper = new creative_lib.ad(creative);
    console.log("Creative initiated, sending back an object with functions.");
    return creativeWrapper;
};


