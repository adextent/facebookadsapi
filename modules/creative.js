/**
 * Created by etayl on 9/21/15.
 * https://developers.facebook.com/docs/marketing-api/adset/pacing/v2.4
 */
var Q = require('q');

exports.creative=function(creative) {
    this.sdk = creative;

    this.getInfo = function (fields) {
        var deferred = Q.defer();
        var creativeInfo = this.sdk.get().require.apply(this, fields);
        creativeInfo.done()
            .then(function (response) {
                console.log("Success with getting info about specific creative, sending back results.");
                deferred.resolve(response.data);
            })
            .catch(function (error) {
                console.log("Troubles with getting creative's info.");
                deferred.reject(error);
            })
        return deferred.promise;
    }


}
