/**
 * Created by etayl on 9/21/15.
 */
var Q = require('q');
var campaign_lib = require('./campaign.js');
var creative_lib = require('./creative.js');
var fs = require('fs');


exports.account = function (adAccount) {
    this.sdk = adAccount;

    this.getInfo = function (fields) {
        var deferred = Q.defer();
        var accountInfo = this.sdk.get().require.apply(this, fields);
        accountInfo.done()
            .then(function (response) {
                console.log("Success with getting info about account, sending back results.");
                deferred.resolve(response.data);
            })
            .catch(function (error) {
                console.log("Troubles with getting account's info.");
                deferred.reject(error);
            })
        return deferred.promise;
    };

    this.getAllCampaignsInfo = function (fields) { //todo: handle paging
        var deferred = Q.defer();
        var adCampaign = this.sdk.getAdCampaigns().require.apply(this, fields);
        adCampaign.done()
            .then(function (response) {
                console.log("Success with getting info about account's campaigns, sending back results.");
                deferred.resolve(response.cache);
            })
            .catch(function (error) {
                console.log("Troubles with getting account's campaigns.");
                deferred.reject(error);
            })
        return deferred.promise;
    };

    this.createCampaign = function (props) {
        var deferred = Q.defer();

        var CreateCampaign = this.sdk.createAdCampaign().set(props);
        CreateCampaign.done()
            .then(function (response) {
                console.log("Success with creating new campaign in account, sending back new campaign object.");
                var campaignWrapper = new campaign_lib.campaign(response.data);
                console.log("Campaign initiated, sending back an object with functions.");
                deferred.resolve(campaignWrapper);
            })
            .catch(function (error) {
                console.log("Troubles with creating campaign in account.");
                deferred.reject(error);
            })
        return deferred.promise;
    };

    this.getAllCreativesInfo = function (fields) { //paging?!?
        var deferred = Q.defer();
        var adCreatives = this.sdk.getCreatives().require.apply(this, fields);
        adCreatives.done()
            .then(function (response) {
               console.log("Success with getting ad's creatives, sending back results.");
                console.log("Paging:"+response.paging+" pageSize:"+response.pageSize+" PageIndex:"+response.pageIndex);

                if (response.paging){
                    paging(response.data,response.next);
                    function paging(data,next){
                        next()
                            .then(function(response){
                                console.log("Paging:"+response.paging+" pageSize:"+response.pageSize+" PageIndex:"+response.pageIndex);
                                console.log("joining..");
                                var alldata=data.concat(response.data);
                                if (response.paging && data.length==25){
                                    paging(alldata,response.next);
                                }
                                else{
                                    deferred.resolve(alldata);
                                }
                            })
                            .catch(function (error) {
                                console.log("Troubles with joining,");
                                deferred.reject(error);
                            })
                    }
                }
                else{
                    deferred.resolve(response.data);
                }

            })
            .catch(function (error) {
                console.log("Troubles with getting campaign's info.");
                deferred.reject(error);
            })
        return deferred.promise;
    }

    this.createCreative = function (props) {
        var deferred = Q.defer();

        var createCreative = this.sdk.createCreative().set(props);
        createCreative.done()
            .then(function (response) {
                console.log("Success with creating new creative in account, sending back new creative object.");
                var creativeWrapper = new creative_lib.creative(response.data);
                console.log("Creative initiated, sending back an object with functions.");
                deferred.resolve(creativeWrapper);
            })
            .catch(function (error) {
                console.log("Troubles with creating creative in account.");
                deferred.reject(error);
            })
        return deferred.promise;
    };

    this.uploadImage = function (props, stream) {

        var deferred = Q.defer();
        var account=this.sdk; //because this is being overwritten
        fs.stat(stream.filename, function (error, stat) {
            var imageStream = fs.createReadStream(stream.filename);
            imageStream.name = props.name;
            imageStream.size = stat.size;

            var UploadedImage = account.createAdImage()
                .set(props)
                .attachFileStream(imageStream);

            UploadedImage.done()
                .then(function (response) {
                    deferred.resolve(response.data.images);
                })
                .catch(function (error) {
                    console.log("Troubles with creating campaign in account.");
                    deferred.reject(error);
                })
        })
        return deferred.promise;
    };

    this.createAd = function (props) {
        var deferred = Q.defer();
        var CreateAd = this.sdk.createAd().set(props);
        CreateAd.done()
            .then(function (response) {
                console.log("Success with creating new ad in account, sending back new ad object.");
                var adWrapper = new ad_lib.ad(response.data);
                console.log("Ad initiated, sending back an object with functions.");
                deferred.resolve(adWrapper);
            })
            .catch(function (error) {
                console.log("Troubles with creating Ad in account.");
                deferred.reject(error);
            })
        return deferred.promise;
    };

}